package com.corvid.genericdto.data.gdto;

import com.corvid.genericdto.shared.time.Duration;
import com.corvid.genericdto.shared.time.TimeUnit;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author mokua
 *         {
 *         "unit": "day",
 *         "quantity": 10
 *         }
 */
public class DurationDeserializer extends JsonDeserializer<Duration> {
    private static Logger log = Logger.getLogger(DurationDeserializer.class.getName());

    @Override
    public Duration deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        Iterator<Map.Entry<String, JsonNode>> fields = node.fields();
        log.info(" All fields for duration " + fields);
        Duration returnValue = null;
        Map.Entry<String, JsonNode> next = fields.next();
        String unitCode = next.getValue().textValue();
        final TimeUnit timeUnit = TimeUnit.of(unitCode);
        //get next
        next = fields.next();
        final long value = next.getValue().longValue();
        log.info(" unit code ' " + timeUnit + " ' , value ' " + value + "' ");

        returnValue = Duration.of(value, timeUnit);
        return returnValue;
    }
}
