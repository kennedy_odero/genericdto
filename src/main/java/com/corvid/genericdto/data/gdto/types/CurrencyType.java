package com.corvid.genericdto.data.gdto.types;

import com.corvid.genericdto.data.gdto.CurrencyDeserializer;
import com.corvid.genericdto.data.gdto.CurrencySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Currency;
import java.util.logging.Logger;

/**
 * Created by mokua on 10/5/17.
 */

@JsonDeserialize(using = CurrencyDeserializer.class)
@JsonSerialize(using = CurrencySerializer.class)
public class CurrencyType extends AbstractType<Currency>{
  private static Logger log = Logger.getLogger(CurrencyType.class.getName());
  private Currency currency;

  public CurrencyType() {

  }

  public CurrencyType(String regExp, String contentAsString) {
    log.info("creating currency type , currency " + contentAsString);
    //super(regExp);
    this.instantiateFromString(contentAsString);
  }

  @Override protected Currency construct(String content) {
    log.info("construct curreny type , code " + content);
    return  Currency.getInstance(content);
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  @Override public String toString() {
    return "CurrencyType{" +
        "currency=" + getValue().getCurrencyCode()+
        '}';
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof CurrencyType)) return false;

    CurrencyType that = (CurrencyType) o;

    return getCurrency() != null ? getCurrency().equals(that.getCurrency())
        : that.getCurrency() == null;
  }

  @Override public int hashCode() {
    return getCurrency() != null ? getCurrency().hashCode() : 0;
  }
}
