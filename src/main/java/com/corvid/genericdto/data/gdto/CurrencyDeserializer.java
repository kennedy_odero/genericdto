package com.corvid.genericdto.data.gdto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.Currency;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author mokua {
 *         {"code": "KES", "name": "Kenyan Shilling" "symbol": "KES }
 */
public class CurrencyDeserializer extends JsonDeserializer<Currency> {
  private static Logger log = Logger.getLogger(CurrencyDeserializer.class.getName());

  @Override
  public Currency deserialize(JsonParser jsonParser, DeserializationContext ctxt)
      throws IOException {
    ObjectCodec oc = jsonParser.getCodec();
    JsonNode node = oc.readTree(jsonParser);
    Iterator<Map.Entry<String, JsonNode>> fields = node.fields();
    log.info(" All fields for currency " + fields);

    Map.Entry<String, JsonNode> next = fields.next();
    String currencyCode = next.getValue().textValue();
    log.info(" currency code ' " + currencyCode);
    final Currency currency = Currency.getInstance(currencyCode);

    return currency;
  }
}
